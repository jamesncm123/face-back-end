import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { AdminModule } from './modules/admin/admin.module';
import { Admin } from './entities/admin.entity';
import { Customer } from './entities/customer.entity';
import { CustomerModule } from './modules/customer/customer.module';
import { CustomerDevice } from './entities/customerDevice.entity';
import { PersonCustomerDevice } from './entities/personCustomerDevice.entity';
import { Device } from './entities/device.entity';
import { ImagePerson } from './entities/imagePerson.entity';
import { Person } from './entities/person.entity';
import { AdminCustomer } from './entities/adminCustomer.entity';
import { EnterOrLeave } from './entities/enterOrLeave.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '1234',
      database: 'IoTFR',
      entities: [
        "dist/**/*.entity{.ts,.js}" || "./*.entity{.ts,.js}"
      ],
      synchronize: true,
      autoLoadEntities: true,
    }),
    AdminModule,
    CustomerModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
