import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class AdminCustomer   {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  customerId: number; 

  @Column()
  adminId: number; 
  
  @Column()
  createdAt: Date;
}