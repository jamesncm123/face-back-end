import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Customer   {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  username: string;

  @Column()
  password: string;

  @Column()
  phoneNumber: string;

  @Column({ default: true })
  isActive: boolean;

  @Column()
  createdAt: Date; 

  @Column()
  createdBy: string; 

  @Column({ default: null })
  updatedAt: Date;
}