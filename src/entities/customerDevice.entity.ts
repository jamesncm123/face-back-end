import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class CustomerDevice   {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  customerId: number; 

  @Column()
  deviceId: number; 
  
  @Column()
  imagePersonId: number;
  
  @Column()
  createdAt: Date; 

}