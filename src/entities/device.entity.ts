import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Device   {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  address: string;

  @Column()
  lat: string;

  @Column()
  long: string;

  @Column()
  status: string;

  @Column()
  customerId: number;

  @Column({ default: true })
  isActive: boolean;
  
  @Column()
  createdAt: Date;
}