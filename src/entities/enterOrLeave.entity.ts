import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class EnterOrLeave   {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string; 
  
  @Column('blob')
  image: any; 
  
  @Column()
  dateTime: Date; 
  
  @Column()
  temperature: number;

  @Column({ default: true })
  isActive: boolean;
  
  @Column()
  createdAt: Date;
}