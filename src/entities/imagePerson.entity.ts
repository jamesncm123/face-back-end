import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ImagePerson   {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  personId: number;

  @Column()
  deviceId: number;

  @Column('blob')
  image: any;
  
  @Column()
  createdAt: Date;
}