import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Person   {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  personType: string;

  @Column()
  manageBy: string;

  @Column()
  phoneNumber: string;

  @Column({ default: true })
  isActive: boolean;
  
  @Column()
  createdAt: Date;

  @Column()
  updatedAt: Date;

  @Column()
  imageId: number;

  @Column()
  status: string;

}