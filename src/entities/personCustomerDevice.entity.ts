import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class PersonCustomerDevice   {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  personId: number; 
  
  @Column()
  personCustomerDevice: number;
  
  @Column()
  createdAt: Date;
}