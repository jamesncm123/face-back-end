import { Controller, Get, Post } from '@nestjs/common';
import { AdminService } from './admin.service';

@Controller('api/v1/admin')
export class AdminController {
    constructor(private service: AdminService){}

    @Get('find')
    async getAdmin(){
        return await this.service.getAdmin();
    }

    @Post('add')
    async insertAdmin(){
        return await this.service.insertAdmin();
    }

    @Post('update')
    async updateAdmin(){
        return await this.service.updateAdmin();
    }
}
