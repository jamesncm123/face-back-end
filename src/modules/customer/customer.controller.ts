import { Controller, Get, Post } from '@nestjs/common';
import { CustomerService } from './customer.service';

@Controller('api/v1/customer')
export class CustomerController {
    constructor(private service: CustomerService){}
    @Get('find')
    async getAdmin(){
        return await this.service.getAdmin();
    }

    @Post('add')
    async insertAdmin(){
        return await this.service.insertAdmin();
    }

    @Post('update')
    async updateAdmin(){
        return await this.service.updateAdmin();
    }
}
