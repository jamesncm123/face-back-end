import { Injectable } from '@nestjs/common';
import { Repository, EntityManager } from 'typeorm';
import { Customer } from 'src/entities/customer.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class CustomerService {
    constructor( @InjectRepository(Customer)
    private adminRepository: Repository<Customer>,
    private entityManager: EntityManager,){}

    async getAdmin(){
        return this.adminRepository.find();
    }

    async insertAdmin(){
        const saveAdmin = {
            firstName: 'james',
            lastName: 'ncm',
            username: 'jamesncm123',
            password: 'password',
            phoneNumber: '+66993276855',
            createdBy: 'admin',
            createdAt: new Date()
        }
       return await this.adminRepository.insert(saveAdmin)
    }

    async updateAdmin(){
        const saveAdmin = {
            firstName: 'james2',
            lastName: 'ncm',
            username: 'jamesncm123',
            password: 'password',
            phoneNumber: '+66993276855',
            updatedAt: new Date()
        }
       return await this.adminRepository.update({id:1},saveAdmin)
    }
    
}
